package com.m0rb1u5.pdm_lab4_4_actividad2;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.preference.DialogPreference;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
public int numMessages = 0;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_main, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }

   public void lanza(View view) {
      NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(this)
                  .setSmallIcon(R.mipmap.ic_launcher)
                  .setContentTitle("Mi notificación")
                  .setContentText("Hola Mundo!");
      Intent resultIntent = new Intent(this, MainActivity.class);
      PendingIntent resultPendingIntent =
            PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
      mBuilder.setContentIntent(resultPendingIntent);
      int mNotificationId = 1;
      NotificationManager mNotifyMgr =
            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
      mNotifyMgr.notify(mNotificationId, mBuilder.build());
   }

   public void cambia(View view) {
      NotificationManager mNotificationManager =
            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
      int notifyID = 1;
      NotificationCompat.Builder mNotifyBuilder =
            new NotificationCompat.Builder(this)
                  .setContentTitle("Nuevo Mensaje")
                  .setContentText("Tienes mensajes nuevos!")
                  .setSmallIcon(R.mipmap.ic_launcher);
      String currentText="Texto";
      mNotifyBuilder.setContentText(currentText).setNumber(++numMessages);
      mNotificationManager.notify( notifyID, mNotifyBuilder.build());
   }

   public void borra(View view) {
      NotificationManager mNotificationManager =
            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
      int notifyID = 1;
      mNotificationManager.cancel(notifyID);
   }

   public void toast(View view) {
      Toast toast = Toast.makeText(getApplicationContext(),"Toast con gravedad" , Toast.LENGTH_SHORT);
      toast.setGravity(Gravity.CENTER|Gravity.LEFT,0,0);
      toast.show();
   }

   public void alerta(View view) {
      FragmentManager fragmentManager = getFragmentManager();
      DialogoAlerta dialogoAlerta = new DialogoAlerta();
      dialogoAlerta.show(fragmentManager, "tagAlerta");
   }

   public void confirmacion(View view) {
      FragmentManager fragmentManager = getFragmentManager();
      DialogoConfirmacion dialogoConfirmacion = new DialogoConfirmacion();
      dialogoConfirmacion.show(fragmentManager, "tagConfirmacion");
   }

   public void snackbarsimple(View view) {
      Snackbar.make(view, "Esto es una prueba", Snackbar.LENGTH_LONG)
            .show();
   }

   public void snackbaraccion(View view) {
      Snackbar.make(view, "Esto es otra prueba", Snackbar.LENGTH_LONG)
            .setActionTextColor(getResources().getColor(R.color.snackbar_action))
            .setAction("Acción", new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                  Log.i("Snackbar","Pulsada acción snackbar!");
               }
            })
            .show();
   }

   public static class DialogoAlerta extends DialogFragment {
      @Override
      public Dialog onCreateDialog(Bundle savedInstanceState) {
         AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
         builder.setMessage("Este es un mensaje de alerta")
               .setTitle("Alerta")
               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                     dialog.cancel();
                  }
               });
         return builder.create();
      }
   }
   public static class DialogoConfirmacion extends DialogFragment {
      @Override
      public Dialog onCreateDialog(Bundle savedInstanceState) {
         AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
         builder.setMessage("¿Confirma la acción seleccionada?")
               .setTitle("Confirmación")
               .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                     Log.i("Dialogos", "Confirmación aceptada.");
                     dialog.cancel();
                  }
               })
               .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                     Log.i("Dialogos", "Confirmación cancelada.");
                     dialog.cancel();
                  }
               });
         return builder.create();
      }
   }
}
